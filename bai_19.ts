class Queue<T>{
    private items: T[];

    constructor() {
        this.items = [];
    }
    // Thêm phần tử vào cuối hàng đợi
    enqueue(element: T): void {
        this.items.push(element);
    }

    // Xóa phần tử ở đầu hàng đợi và trả về nó
    dequeue(): T | undefined {
        return this.items.shift();
    }

    // Xem phần tử ở đầu hàng đợi mà không xóa nó
    peek(): T | undefined {
        return this.items.length === 0 ? undefined : this.items[0];
    }

    // Lấy số lượng phần tử trong hàng đợi
    size(): number {
        return this.items.length;
    }

    // Xóa tất cả các phần tử trong hàng đợi
    clear(): void {
        this.items = [];
    }

    // Kiểm tra hàng đợi có trống không
    isEmpty(): boolean {
        return this.items.length === 0;
    }
}

const queue = new Queue<number>();
queue.enqueue(1);
queue.enqueue(5);
queue.enqueue(10);
queue.enqueue(15);

console.log(queue.dequeue());
console.log(queue.peek());
console.log(queue.size());
queue.clear();
console.log(queue.isEmpty()); 