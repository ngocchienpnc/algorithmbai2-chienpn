class Products {
    name: string;
    price: number;
    quality: number;
    categoryId: number;
    constructor(name: string, price: number, quality: number, categoryId: number) {
        this.name = name;
        this.price = price;
        this.quality = quality;
        this.categoryId = categoryId;
    }
}

let products: Products[] = [
    { name: 'CPU', price: 750, quality: 10, categoryId: 1 },
    { name: 'RAM', price: 50, quality: 2, categoryId: 2 },
    { name: 'HDD', price: 70, quality: 1, categoryId: 2 },
    { name: 'Main', price: 400, quality: 3, categoryId: 1 },
    { name: 'Keyboard', price: 30, quality: 8, categoryId: 4 },
    { name: 'Mouse', price: 25, quality: 50, categoryId: 4 },
    { name: 'VGA', price: 60, quality: 35, categoryId: 3 },
    { name: 'Monitor', price: 120, quality: 28, categoryId: 2 },
    { name: 'Case', price: 120, quality: 28, categoryId: 5 }
]

class Categories {
    id: number;
    name: string;

    constructor(id: number, name: string) {
        this.id = id;
        this.name = name;
    }
}

let categories: Categories[] = [
    { id: 1, name: 'Comuter' },
    { id: 2, name: 'Memory' },
    { id: 3, name: 'Card' },
    { id: 4, name: 'Acsesory' }
]

class ProductDetail {
    productName: string;
    price: number;
    quality: number;
    categoryID: number;
    categoryName: string

    constructor(productName: string, price: number, quality: number, categoryID: number, categoryName: string) {
        this.productName = productName;
        this.price = price;
        this.quality = quality;
        this.categoryID = categoryID;
        this.categoryName = categoryName
    }
}
function mapProductByCategory(productList: Products[], categoryList: Categories[],): ProductDetail[] {
    let productListResut: ProductDetail[] = [];

    for (let product of productList) {

        let category = categoryList.find(categories => categories.id === product.categoryId);

        if (category?.name !== undefined) {
            productListResut.push({
                productName: product.name,
                price: product.price,
                quality: product.quality,
                categoryID: product.categoryId,
                categoryName: category?.name
            })
        }
    }
    return productListResut;
}

let productByCategory = mapProductByCategory(products, categories)
console.log(productByCategory);

