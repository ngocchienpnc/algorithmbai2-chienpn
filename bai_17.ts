//Đệ quy
//--Tính giai thừa--
function calculateFactorial(n: number): number {
    if (n === 0){
        return 1
    }
    return n* calculateFactorial(n-1);
}

let n = calculateFactorial(10)
console.log(n);