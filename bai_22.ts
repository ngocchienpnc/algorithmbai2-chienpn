
//---Sử dụng đệ quy
function calMonthRecursive(money: number, rate: number, target: number, months: number) {
    if (money >= target) {
        return months;
    }
    else {
        let interest = money * (rate / 100)

        return calMonthRecursive(money + interest, rate, target, months + 1)
    }
}
function calMonthTarget(money: number, rate: number) {
    let target = money * 2
    return calMonthRecursive(money, rate, target, 0);
}

const result = calMonthTarget(1000, 5);
console.log(`Số tháng cần để tiền gốc và lãi tăng gấp đôi là ${result} tháng.`);

//--Không sử dụng dệ quy
function calMonth(money: number, rate: number) {
    let target = money * 2;


    let months = 0;
    while (money < target) {
        let interest = money * (rate / 100);

        money += interest;


        months++;
    }
    return months
}

let calMonthResult = calMonth(1000, 5);

console.log(`Số tháng cần để tiền gốc và lãi tăng gấp đôi là ${calMonthResult} tháng.`);