
//-- sử dụng đệ quy
function calSalary(salary: number, year: number = 0): number {
    if (year <= 0) {
        return salary;
    }
    return calSalary(salary, year - 1) * 1.1;
}

let salary = calSalary(5000000, 2)
console.log('----');

console.log(salary);


//Không sử dụng đệ quy

function caSalary(salary: number, year: number) {
    for (let i = 0; i < year; i++) {
        salary *= 1.1
    }
    return salary
}

let cSalary = caSalary(5000000, 2)
console.log('----');
console.log(cSalary);
