class Product {
    name: string;
    price: number;
    quality: number;
    categoryId: number;
    constructor(name: string, price: number, quality: number, categoryId: number) {
        this.name = name;
        this.price = price;
        this.quality = quality;
        this.categoryId = categoryId;
    }
}

let product: Product[] = [
    { name: 'CPU', price: 750, quality: 10, categoryId: 1 },
    { name: 'RAM', price: 50, quality: 2, categoryId: 2 },
    { name: 'HDD', price: 70, quality: 1, categoryId: 2 },
    { name: 'Main', price: 400, quality: 3, categoryId: 1 },
    { name: 'Keyboard', price: 30, quality: 8, categoryId: 4 },
    { name: 'Mouse', price: 25, quality: 50, categoryId: 4 },
    { name: 'VGA', price: 60, quality: 35, categoryId: 3 },
    { name: 'Monitor', price: 120, quality: 28, categoryId: 2 },
    { name: 'Case', price: 120, quality: 28, categoryId: 5 }
]

class Category {
    id: number;
    name: string;

    constructor(id: number, name: string) {
        this.id = id;
        this.name = name;
    }
}

let category: Category[] = [
    { id: 1, name: 'Comuter' },
    { id: 2, name: 'Memory' },
    { id: 3, name: 'Card' },
    { id: 4, name: 'Acsesory' }
]

function sortByCategoryName(product: Product[], category: Category[]): Product[] {

    let sortedProducts = [...product]

    for (var i = 0; i < product.length; i++) {

        let currentProduct = product[i];
        let j = i - 1;

        const currentCategory = category.find(category => category.id === currentProduct.categoryId);

        while (j >= 0 && currentCategory && sortedProducts[j].name !== undefined) {

            let previousCategory = category.find(category => category.id === sortedProducts[j].categoryId);

            if (previousCategory && currentCategory.name < previousCategory.name) {
                sortedProducts[j + 1] = sortedProducts[j];
                j--;
            } else {
                break;
            }
        }
        sortedProducts[j + 1] = currentProduct;
    }
    return sortedProducts;
}

let sortByCategoryname = sortByCategoryName(product, category)
console.log(category);
console.log(sortByCategoryname);

///bài 15 Min Price
function minByPrice(productList: Product[]): Product | null {

    if (productList.length === 0) {
        return null;
    }
    let minProduct = productList[0];

    for (let i = 1; i < productList.length; i++) {
        if (productList[i].price < minProduct.price) {
            minProduct = productList[i];
        }
    }
    return minProduct;
}
let minProductResut = minByPrice(product);
console.log(minProductResut);

///Bài 16 Max by price

function maxByPrice(productList: Product[]) {
    let maxByPrice = productList[0]

    for (let i = 1; i < productList.length; i++) {
        if (productList[i].price > maxByPrice.price) {
            maxByPrice = productList[i];
        }
    }
    return maxByPrice;
}

let maxProductResut = maxByPrice(product);

console.log(maxProductResut);