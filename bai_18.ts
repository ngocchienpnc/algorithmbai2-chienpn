function fibonacci(n: number): number {
    if (n <= 1) {
        return n;
    } else {
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}
for (let i = 0; i <= 10; i++) {
    console.log(`F(${i}) = ${fibonacci(i)}`);
}

function fibonacciPopulationCycle(generations: number): number {
    if (generations <= 1) {
        return generations;
    } else {
        // Tính số lượng cá thể theo dãy Fibonacci sau mỗi chu kỳ sinh sản
        return fibonacciPopulationCycle(generations - 1) + fibonacciPopulationCycle(generations - 2);
    }
}

let numberOfGenerations = 5;
let population = fibonacciPopulationCycle(numberOfGenerations);
console.log(`Số lượng cá thể sau ${numberOfGenerations} chu kỳ sinh sản là ${population}`);

